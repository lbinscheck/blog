"use strict";

require("dotenv").config({
  path: `.env`
});

module.exports = {
  siteMetadata: {
    title: "Reseptit",
    description: "Recipes",
    keywords: "gatsbyjs, gatsby, javascript, binscheck, blog",
    siteUrl: "https://reseptit.netlify.com",
    author: {
      name: "Lennart Binscheck",
      email: "binschecklennart@gmail.com"
    }
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        // You can add multiple tracking ids and a pageview event will be fired for all of them.
        trackingIds: [
          process.env.GA_ID // Google Analytics / GA
        ],
        // This object is used for configuration specific to this plugin
        pluginConfig: {
          // Puts tracking script in the head instead of the body
          head: true,
          // Setting this parameter is also optional
          respectDNT: true
        }
      }
    },
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          {
            resolve: "gatsby-remark-responsive-iframe",
            options: {
              wrapperStyle: "margin-bottom: 1rem"
            }
          },
          "gatsby-remark-prismjs",
          "gatsby-remark-copy-linked-files",
          "gatsby-remark-smartypants",
          {
            resolve: "gatsby-remark-images",
            options: {
              maxWidth: 1140,
              quality: 90,
              linkImagesToOriginal: false
            }
          }
        ]
      }
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: process.env.CONTENTFUL_SPACE_ID,
        accessToken: process.env.CONTENTFUL_ACCESSTOKEN
      }
    },
    "gatsby-transformer-json",
    {
      resolve: "gatsby-plugin-canonical-urls",
      options: {
        siteUrl: "https://reseptit.netlify.com"
      }
    },
    `gatsby-plugin-styled-components`,
    "gatsby-plugin-typescript",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    "gatsby-plugin-react-helmet",
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Reseptit`,
        short_name: `Reseptit`,
        description: `Bjugg's best food recipes. Enjoy!`,
        lang: `en`,
        display: `standalone`,
        icon: `src/images/voldi.webp`,
        start_url: `/`,
        background_color: `#9d7cbf`,
        theme_color: `#9d7cbf`
      }
    },
    "gatsby-plugin-offline"
  ]
};
