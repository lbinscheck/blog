import React from "react";
import styled from "styled-components";
import Header from "../components/Header";

const StyledLayoutMain = styled.main`
  display: grid;
  position: relative;
  @media (min-width: 641px) {
    width: 100%;
    max-width: 1300px;
    margin: 0 auto;
  }
`;

interface LayoutMainProps {
  className?: string;
  title: string;
}

const LayoutMain: React.FC<LayoutMainProps> = ({ children, className }) => {
  return (
    <StyledLayoutMain className={className}>
      <Header />
      {children}
    </StyledLayoutMain>
  );
};

export default LayoutMain;
