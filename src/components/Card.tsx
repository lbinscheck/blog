import React from "react";
import styled from "styled-components";
import { Link } from "gatsby";
import Image from "gatsby-image";
import { colors } from "../styles/variables";

const StyledCard = styled(Link)`
  display: grid;
  position: relative;
  width: fit-content;
  & img {
    overflow: hidden;
    border-radius: 12px;
    object-fit: cover;
  }
`;

const Gradient = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  background: rgb(0, 0, 0);
  background: linear-gradient(0deg, rgba(0, 0, 0, 1) 0%, rgba(0, 0, 0, 0) 30%);
  z-index: 1;
  border-radius: 12px;
`;

const TextWrapper = styled.div`
  color: ${colors.white};
  justify-self: start;
  align-self: end;
  position: absolute;
  margin: 10px;
  z-index: 2;
`;

const StyledTitle = styled.h1`
  font-size: 14px;
  font-weight: 500;
  margin: 0;
`;

const Placeholder = styled.div<{ color: string }>`
  height: 200px;
  width: 250px;
  border-radius: 12px;
  background-color: ${props => props.color};
`;

const StyledImage = styled(Image)``;

type CardProps = {
  image: any;
  title: string;
  slug: string;
};

const Card: React.FC<CardProps> = ({ image, title, slug }) => {
  const colours = ["#d1aaf0", colors.brand, "#aaf0d1", "#f0aac9"];
  return (
    <StyledCard to={`/${slug}`}>
      <Gradient />
      {image ? <StyledImage fixed={image} fadeIn={false} /> : <Placeholder color={colours[Math.floor(Math.random() * colours.length)]} />}
      <TextWrapper>
        <StyledTitle>{title}</StyledTitle>
      </TextWrapper>
    </StyledCard>
  );
};
export default Card;
