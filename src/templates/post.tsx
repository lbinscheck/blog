import React from "react";
import { graphql, Link } from "gatsby";
import IndexLayout from "../layouts";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { BLOCKS } from "@contentful/rich-text-types";
import styled from "styled-components";
import Image from "gatsby-image";
import useContentfulImage from "../hooks/useContentfulImage";
import { CategoryContainer, CategoryTitle, Category, Container, Wrapper } from "../pages";
import Card from "../components/Card";

const StyledPost = styled.div`
  display: grid;
  max-width: 500px;
  width: 100%;
  grid-gap: 10px;
  margin: 0 auto 20px auto;
  p {
    color: rgba(0, 0, 0.9);
    margin: 0 20px;
  }
  ul {
    color: pink;
  }
  li p {
    font-weight: bold;
    color: rgba(0, 0, 0.8);
    margin: 5px 0;
  }
`;
const StyledPostTitle = styled.h1`
  font-size: 2.5rem;
  font-weight: 400;
  justify-self: center;
`;
const StyledLink = styled(Link)`
  font-size: 1rem;
  font-weight: 300;
  color: darkgray;
  justify-self: center;
`;
const StyledPostHeadline = styled.div`
  display: grid;
  margin: 20px 0 20px 0;
`;
const Post: React.FC = ({ data }: any) => {
  return (
    <IndexLayout>
      <StyledPost>
        <StyledPostHeadline>
          <StyledPostTitle>{data.contentfulPost.title}</StyledPostTitle>
          <StyledLink to={`/author/${data.contentfulPost.author.slug}`}>{data.contentfulPost.author.fullName}</StyledLink>
        </StyledPostHeadline>
        {documentToReactComponents(data.contentfulPost.body.json, {
          renderNode: {
            [BLOCKS.EMBEDDED_ASSET]: node => {
              const fluid = useContentfulImage(node.data.target.fields.file["en-US"].url);
              return <Image title={node.data.target.fields.title["en-US"]} fluid={fluid} fadeIn={false} />;
            }
          }
        })}
      </StyledPost>
      <Container>
        <Wrapper>
          <CategoryContainer>
            <CategoryTitle>{data.contentfulPost.category.category}</CategoryTitle>
            <Category>
              {data.contentfulPost.category.post.map((post: any) => (
                <Card title={post.title} slug={post.slug} image={post.image && post.image.fixed} key={post.slug} />
              ))}
            </Category>
          </CategoryContainer>
        </Wrapper>
      </Container>
    </IndexLayout>
  );
};

export default Post;

export const query = graphql`
  query postQuery($slug: String!) {
    contentfulPost(slug: { eq: $slug }) {
      title
      body {
        json
      }
      author {
        fullName
        slug
      }
      category {
        category
        post {
          title
          slug
          image {
            fixed(width: 250, height: 200) {
              ...GatsbyContentfulFixed_withWebp
            }
          }
        }
      }
    }
  }
`;
