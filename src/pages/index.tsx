import React, { useEffect, useState, useRef } from "react";
import { graphql } from "gatsby";
import styled from "styled-components";
import IndexLayout from "../layouts";
import Card from "../components/Card";
import { colors } from "../styles/variables";
import { Search } from "@styled-icons/octicons";

export const Container = styled.div`
  display: grid;
  margin-top: 20px;
  margin-bottom: 20px;
  padding-left: 10px;
  padding-right: 10px;
  @media (max-width: 641px) {
    padding-right: 0;
  }
`;

export const Category = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
  grid-gap: 5px;
  @media (max-width: 641px) {
    display: grid;
    grid-template-columns: none;
    grid-auto-flow: column;
    justify-content: start;
    grid-gap: 5px;
    overflow: scroll;
    width: 100vw;
    margin-left: -10px;
    scrollbar-width: none;
    ::-webkit-scrollbar {
      display: none;
    }
    &::before {
      content: "";
      width: 10px;
    }
    &::after {
      content: "";
      width: 10px;
    }
  }
`;

export const Wrapper = styled.div`
  display: grid;
  grid-gap: 30px;
`;

const PageTitle = styled.h1`
  color: ${colors.gray.calm};
  @media (min-width: 641px) {
    font-size: 55px;
  }
`;

export const CategoryContainer = styled.div`
  display: grid;
  grid-gap: 5px;
`;

export const CategoryTitle = styled.h2`
  margin: 5px;
  color: ${colors.lilac};
`;

const SearchWidget = styled.div<{ isSearch: boolean }>`
  display: grid;
  grid-auto-flow: column;
  justify-content: center;
  align-items: center;
  grid-gap: 4px;
  padding: ${props => (props.isSearch ? `0 10px` : `0`)};
  border-radius: 50px;
  height: 50px;
  min-width: 50px;
  background-color: ${colors.brand};
  color: ${colors.accent};
  top: 40px;
  right: 5%;
  position: fixed;
  z-index: 10000;
`;

const SearchField = styled.input`
  outline: none;
  border: none;
  background-color: ${colors.brand};
  color: ${colors.accent};
  width: 100px;
`;

const CategoryChips = styled.div`
  display: grid;
  grid-auto-flow: column;
  justify-content: start;
  justify-items: start;
  grid-gap: 5px;
  @media (max-width: 641px) {
    overflow: scroll;
    scrollbar-width: none;
    ::-webkit-scrollbar {
      display: none;
    }
    width: 100vw;
    margin-left: -10px;
    &::before {
      content: "";
      width: 10px;
    }
    &::after {
      content: "";
      width: 10px;
    }
  }
`;

const CategoryChip = styled.div<{ isSelected: boolean }>`
  ${props => (props.isSelected ? `background-color: #b52212;` : `background-color: #fa6555;`)}
  padding: 10px 20px;
  border-radius: 30px;
  color: #fff;
  cursor: pointer;
`;

const IndexPage = ({ data }: any) => {
  const [value, setValue] = useState("");
  const [isSearch, setIsSearch] = useState(false);
  const inputEl = useRef<HTMLInputElement>(null);
  const [selectedCategory, setSelectedCategory] = useState<string>("All");

  const categories = data.allContentfulCategory.edges.filter(({ node }: any) => node.post);
  const sortedCategories = categories.sort((a: any, b: any) => b.node.post.length - a.node.post.length);
  const sorted = sortedCategories.map(({ node }: any) => {
    return { category: node.category, posts: [...node.post.filter((p: any) => p.title.toLowerCase().includes(value.toLowerCase()))] };
  });
  const filtered = sorted.filter((f: any) => {
    if (selectedCategory === "All") {
      return true;
    } else {
      return f.category === selectedCategory;
    }
  });

  const handleSearch = (event: any) => {
    setValue(event.target.value);
  };

  const startSearch = () => {
    setIsSearch(true);
  };

  useEffect(() => {
    if (inputEl && inputEl.current) {
      inputEl.current.focus();
    }
  }, [isSearch]);

  return (
    <>
      <SearchWidget isSearch={isSearch} onClick={() => startSearch()} onBlur={() => setIsSearch(false)}>
        <Search size="20" />
        {isSearch && <SearchField ref={inputEl} value={value} onChange={handleSearch} type="text" />}
      </SearchWidget>
      <IndexLayout>
        <Container>
          <Wrapper>
            <PageTitle>Reseptit</PageTitle>
            <CategoryChips>
              <CategoryChip onClick={() => setSelectedCategory("All")} isSelected={selectedCategory === "All"}>
                All
              </CategoryChip>
              {sorted.map((node: any, index: number) => (
                <CategoryChip
                  key={index}
                  onClick={() => setSelectedCategory(node.category)}
                  isSelected={selectedCategory === node.category}
                >
                  {node.category}
                </CategoryChip>
              ))}
            </CategoryChips>
            {filtered.map((node: any) => (
              <>
                {node.posts.length > 0 && (
                  <CategoryContainer key={node.category}>
                    <CategoryTitle>{node.category}</CategoryTitle>
                    <Category>
                      {node.posts.map((post: any) => (
                        <Card title={post.title} slug={post.slug} image={post.image && post.image.fixed} key={post.slug} />
                      ))}
                    </Category>
                  </CategoryContainer>
                )}
              </>
            ))}
          </Wrapper>
        </Container>
      </IndexLayout>
    </>
  );
};

export default IndexPage;

export const query = graphql`
  {
    allContentfulCategory {
      edges {
        node {
          category
          post {
            title
            slug
            image {
              fixed(width: 250, height: 200) {
                ...GatsbyContentfulFixed_withWebp
              }
            }
          }
        }
      }
    }
  }
`;
