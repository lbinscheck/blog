# Reseptit

Little blog to find our best recipes.   
Easy to add and update recipes on Contentful.

* GatsbyJs
* TypeScript
* styled-components
* Contentful
* GraphQL
* Netlify